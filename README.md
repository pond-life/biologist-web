# biologist-web

![Stable life](stable-life.gif)

Provides an Angular interface to a [biologist](https://gitlab.com/pond-life/biologist) server allowing the user to create instances of [Conway's Game of Life](http://www.conwaylife.com/wiki/Conway%27s_Game_of_Life) for analysis.
