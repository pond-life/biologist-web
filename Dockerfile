### Build the code
FROM golang:1.12-alpine
ENV CGO_ENABLED=0 GO111MODULE=on GOOS=linux GOARCH=amd64
RUN apk add --update git
RUN go install -v -ldflags="-w -s" git.sr.ht/~hokiegeek/biologist/cmd/biologistd

### Package it up
FROM alpine
EXPOSE 80 8080 443
COPY --from=0 /go/bin/biologistd /
COPY dist/ /web/
ENTRYPOINT ["/biologistd"]
