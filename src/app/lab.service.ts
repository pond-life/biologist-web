import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';

import { Cell } from './cell';

const maxGenerationsPerExperiment = 500;
// const maxGenerationsPerPoll = 25
const server = environment.biologistUrl;

class Dimensions {
    width: number;
    height: number;
}

class Location {
    X: number;
    Y: number;
}

class CreateAnalysisResponse {
    ID: string;
    dims: Dimensions;
}

class AnalysisUpdate {
    ID: string;
    Dims: Dimensions;
    Status: string;
    Generation: number;
    Living: Location[];
}

class AnalysisUpdateResponse {
    ID: string;
    Updates: AnalysisUpdate[];
}

export class Generation {
    num: number;
    status: string;
    living: Cell[];

    constructor(n, s, l) {
        this.num = n;
        this.status = s;
        this.living = l;
    }
}

const pollRateMs = 1000;
const maxGenerationsPerPoll = 25;

export class Experiment {
    public id: string;
    public initialDensity: number;
    public autoStart: boolean;
    public seed: Generation;
    public generations: { [gen: number]: Generation } = {};
    public maxGenerations: number;
    private _rows: number;
    private _columns: number;
    private lowGeneration = 0;
    private highGeneration = 0;
    private isPolling = true;
    private poller;

    set rows(r: number) {
        this._rows = +r;
    }

    get rows(): number {
        return this._rows;
    }

    set columns(c: number) {
        this._columns = +c;
    }

    get columns(): number {
        return this._columns;
    }

    constructor(private lab: Lab, rows: number, columns: number, density: number, autoStart: boolean, maxGenerations: number) {
        this.rows = rows;
        this.columns = columns;
        this.initialDensity = density;
        this.autoStart = autoStart;
        this.maxGenerations = maxGenerations;
    }

    private create() {
        this.lab.createExperiment(this.columns, this.rows, this.seed.living)
            .subscribe(response => {
                    // console.log(">> ANALYZE CALLBACK", response)
                    this.id = response.ID;

                    this.lab.control(this.id, 0);
                    this.poller = setInterval(() => {
                            if (this.isPolling) {
                                this.lab.poll(this.id, this.highGeneration, maxGenerationsPerPoll)
                                    .subscribe(updates => {
                                            for (const update of updates.Updates) {
                                                if (update != null) {
                                                    // console.log(">> POLL CALLBACK", update)
                                                    if (update.Generation < this.lowGeneration) {
                                                        this.lowGeneration = update.Generation;
                                                    }
                                                    if (update.Generation > this.highGeneration) {
                                                        this.highGeneration = update.Generation;
                                                    }
                                                    this.generations[update.Generation] =
                                                        new Generation(update.Generation, update.Status, update.Living);
                                                }
                                            }
                                        }
                                    );
                            }
                        }, pollRateMs
                        );
                }
            );
    }

    start() {
        if (this.id == null) {
            this.create();
        }
        this.isPolling = true;
    }

    stop() {
        this.isPolling = false;
    }

    incinerate() {
        this.stop();
        clearInterval(this.poller);
        this.lab.removeExperiment(this);
        this.lab.control(this.id, 2);
    }

    numGenerations(): number {
        return Object.keys(this.generations).length;
    }

    get(num: number): Generation {
        // console.log("get: ", num)
        if (num in this.generations) {
            return this.generations[num];
        } else if (this.numGenerations() === 0 && this.seed != null) {
            return this.seed;
        } else {
            // console.log('oh oh: get(): ', num, this.generations);
            return null;
        }
        // TODO if num is outside the boundaries (lowGen, highGen), poll for gen 'num' as well as a buffer around it
        // -> what about the boundaries? should I poll for the space between highGen and num+buffer? What if that is huge?
        // return this.generations.
    }
}

@Injectable()
export class Lab {
    experiments: Experiment[] = [];

    constructor(private http: HttpClient) { }

    newExperiment(rows: number, cols: number, density: number, autostart: boolean) {
        this.experiments.push(new Experiment(this, rows, cols, density, autostart, maxGenerationsPerExperiment));
    }

    removeExperiment(experiment: Experiment) {
        const index: number = this.experiments.indexOf(experiment);
        if (index > -1) {
            this.experiments.splice(index, 1);
            // this.lab.control(this.id, 1)
        }
    }

    createExperiment(width, height, seed): Observable<CreateAnalysisResponse> {
        const req = {Dims: {Width: width, Height: height}, Pattern: 0, Seed: seed};
        return this.http.post<CreateAnalysisResponse>(server + '/analyze', JSON.stringify(req));
    }

    poll(id, startingGen, maxGen): Observable<AnalysisUpdateResponse> {
        const req = {'ID': id, 'StartingGeneration': startingGen, 'NumMaxGenerations': maxGen};
        return this.http.post<AnalysisUpdateResponse>(server + '/poll', JSON.stringify(req));
    }

    control(id, order) {
        this.http.post(server + '/control', JSON.stringify({'ID': id, 'Order': order})).subscribe();
    }
}
