import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { PetridishComponent } from './petridish.component';
import { BoardComponent } from './board/board.component';
import { CultureInfoComponent } from './culture-info/culture-info.component';

import { Lab, Experiment } from '../lab.service';

describe('PetridishComponent', () => {
  let component: PetridishComponent;
  let fixture: ComponentFixture<PetridishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
      ],
      declarations: [
          PetridishComponent,
          BoardComponent,
          CultureInfoComponent
      ],
      providers: [Lab]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetridishComponent);
    component = fixture.componentInstance;
    component.experiment = new Experiment(TestBed.get(Lab), 42, 42, 42, false, 42);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

// @Component({
//   selector: 'petridish',
//   templateUrl: './petridish.component.html',
//   styleUrls: ['./petridish.component.css']
// })
// export class PetridishComponent implements OnInit {

