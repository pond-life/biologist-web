import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CultureInfoComponent } from './culture-info.component';
import { Cell } from '../../cell';
import { Generation } from '../../lab.service';

describe('CultureInfoComponent', () => {
  let component: CultureInfoComponent;
  let fixture: ComponentFixture<CultureInfoComponent>;
  const dummyGeneration = new Generation(42, 'dummy', []);
  dummyGeneration.living.push(new Cell(12, 34));
  dummyGeneration.living.push(new Cell(42, 24));
  dummyGeneration.living.push(new Cell(1, 10));
  dummyGeneration.living.push(new Cell(2, 20));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultureInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CultureInfoComponent);
    component = fixture.componentInstance;
    component.generation = dummyGeneration;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
