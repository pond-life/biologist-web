import { Component, OnInit } from '@angular/core';

import { Lab } from '../lab.service';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent implements OnInit {
    public defaultRows = 50;
    public defaultColumns = 75;
    public defaultDensity = 60;
    public autoStart = true;

    constructor(private lab: Lab) { }

    ngOnInit() { }

    create() {
        this.lab.newExperiment(Number(this.defaultRows), Number(this.defaultColumns),
                               Number(this.defaultDensity), this.autoStart);
    }
}
