import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ControlsComponent } from './controls/controls.component';
import { PetridishComponent } from './petridish/petridish.component';
import { BoardComponent } from './petridish/board/board.component';
import { CultureInfoComponent } from './petridish/culture-info/culture-info.component';
import { DishrackComponent } from './dishrack/dishrack.component';

import { Lab } from './lab.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ControlsComponent,
        PetridishComponent,
        BoardComponent,
        CultureInfoComponent,
        DishrackComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule,
      ],
      providers: [Lab],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

    /*
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
     */
});
