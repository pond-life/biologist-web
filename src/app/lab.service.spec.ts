import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { Lab } from './lab.service';

describe('Lab', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [Lab]
    });
  });

  it('should be created', inject([Lab], (service: Lab) => {
    expect(service).toBeTruthy();
  }));
});
