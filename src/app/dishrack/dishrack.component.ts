import { Component, OnInit } from '@angular/core';

import { Lab } from '../lab.service';
import { Cell } from '../cell';

@Component({
  selector: 'app-dishrack',
  templateUrl: './dishrack.component.html',
  styleUrls: ['./dishrack.component.css']
})
export class DishrackComponent implements OnInit {
    constructor(public lab: Lab) { }

    ngOnInit() { }
}
