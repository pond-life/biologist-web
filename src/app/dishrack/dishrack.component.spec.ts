import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { PetridishComponent } from '../petridish/petridish.component';
import { BoardComponent } from '../petridish/board/board.component';
import { CultureInfoComponent } from '../petridish/culture-info/culture-info.component';

import { Lab } from '../lab.service';

import { DishrackComponent } from './dishrack.component';

describe('DishrackComponent', () => {
  let component: DishrackComponent;
  let fixture: ComponentFixture<DishrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            FormsModule,
            HttpClientModule
        ],
        declarations: [
            DishrackComponent,
            BoardComponent,
            CultureInfoComponent,
            PetridishComponent
        ],
        providers: [Lab],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
