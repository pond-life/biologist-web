import { Component, OnInit } from '@angular/core';

import { Lab } from './lab.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(public lab: Lab) { }

    ngOnInit() { }
}
